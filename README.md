# Animation of DOC Production

This repository has the scripts of the animation of the soil water movement and dissolve organic carbon production in the zhurucay catchment.

> Data are not available.

## Result

![](zhu-animation.gif)
